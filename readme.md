### Game of life

Najprv som chcel zobrazovať výsledky v canvase, pretože som nevedel aký veľký bude "world" a canvas sa dá zoomovať (preto vytvorený controller a formulár). Tam je ale problém s payloadom, takže nakoniec som sa predsa len rozhodol pre CLI.

Tiež som vytvoril Validator, ktorý kontroluje xml schema, táto validácia je ale len naznačená, nevenoval som pozornosť vytvoreniu xsd súboru.

Keďže objekty, s ktorými pracujem nie sú entity (nemajú identifikátor) ani DTO (neslúžia k prenosu dát), sú to tzv value objekty, preto sú v namespace "ValueObject". Mali by byť immutable, keďže ale Matrix mení svoju štruktúru po každom kroku výpočtu, má 2 settery.

Použil som objekty v inicializačných procesoch kvôli jednoduchšej validácii. Vo výpočtoch ale používam asociatívne arrays, pretože ich tvorba je rýchlejšia.

Simulácia sa dá spustiť príkazom `php bin/console app:simulate <nazov_suboru> -m 1000`, kde "-m" je počet milisekúnd spánku programu medzi krokmi výpočtu, aby bolo čo sledovať.

Testy spustíte príkazom `php bin/phpunit`.

Testoval som kroky výpočtu simulácie, parsery by sa samozrejme mohli otestovať tiež. V testoch som najskôr veľa mockoval, nakoniec som ale kód prepísal tak, aby bol jednoducho testovateľný aj bez toho.

Tvorbu ValueObjectov som validoval Validator komponentom za cenu výkonu (Symfony Validator nie je určený pre veľké množstvo validácií). Ak budete skúšať nejaké veľké súbory, odporúčam zakomentovať použitie Validator komponenty v *MatrixParser* a *WorldInfoParser*.

Snád je to všetko, dúfam, že sa Vám bude implementácia pozdávať.