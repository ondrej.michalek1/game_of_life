<?php

namespace App\Tests;

use App\WorldSimulation\ReplicationRuleResolver;
use PHPUnit\Framework\TestCase;
use ReflectionClass;
use ReflectionException;
use ReflectionMethod;

class PrivateMethodTestCase extends TestCase
{
    /**
     * @throws ReflectionException
     */
    protected static function callPrivateMethod(Object $object, string $name, array $args): mixed
    {

        $class = new ReflectionClass($object);
        $privateMethod = $class->getMethod($name);

        return $privateMethod->invokeArgs($object, $args);
    }
}