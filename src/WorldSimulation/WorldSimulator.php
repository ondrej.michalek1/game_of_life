<?php

namespace App\WorldSimulation;

use App\Enum\ReplicationResult;
use App\ValueObject\Matrix;
use Generator;

use function Symfony\Component\String\b;

class WorldSimulator
{

    public function __construct(private readonly ReplicationRuleResolver $replicationRuleResolver)
    {
    }

    public function getSteps(Matrix $matrix): Generator
    {

        for ($i = 0; $i < $matrix->getWorldInfo()->getIterationsCount(); $i++) {
            $step = $this->step($matrix);
            $matrix->setMatrix($step);
            yield $step;
        }
    }

    private function step(Matrix $matrix): array
    {

        $nextStepMatrix = $matrix->getMatrix();
        $newOccupiedIndices = [];
        foreach ($this->getPossibleChangeCells($matrix) as $cell) {
            $speciesToOccupy = $this->replicationRuleResolver->resolve($matrix, $cell['x'], $cell['y']);

            if (count($speciesToOccupy) === 0) {
                $nextStepMatrix[$cell['y']][$cell['x']] = '';
                continue;
            }
            $nextStepMatrix[$cell['y']][$cell['x']] = $speciesToOccupy[array_rand($speciesToOccupy)];
            $newOccupiedIndices[] = $cell;
        }
        $matrix->setOccupiedIndices($newOccupiedIndices);

        return $nextStepMatrix;
    }

    /**
     * @param Matrix $matrix
     * @return array
     * Returns only those indices of matrix that are worth checking for changes. A union of every occupied cell and their neighborhoods that is.
     */
    private function getPossibleChangeCells(Matrix $matrix): array
    {

        $possibleChangeCells = [];
        foreach ($matrix->getOccupiedIndices() as $occupiedIndex) {
            $localPossibleChangeCells = $matrix->generateNeighborhoodIndices($occupiedIndex['x'], $occupiedIndex['y']);
            $localPossibleChangeCells[] = $occupiedIndex;

            foreach ($localPossibleChangeCells as $cell) {
                if (!in_array($cell, $possibleChangeCells)) {
                    $possibleChangeCells[] = $cell;
                }
            }
        }

        return $possibleChangeCells;
    }
}