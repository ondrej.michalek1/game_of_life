<?php

namespace App\WorldSimulation;

use App\Enum\ReplicationResult;
use App\ValueObject\Matrix;

class ReplicationRuleResolver
{

    /**
     * @param Matrix $matrix
     * @param int $x
     * @param int $y
     * @return string[]
     * returns species that would like to occupy specified x, y
     */
    public function resolve(Matrix $matrix, int $x, int $y): array
    {

        $neighborhood = $matrix->getNeighborhood($x, $y);
        $speciesToOccupy = [];

        foreach ($neighborhood as $species => $speciesCount) {
            $rule = $this->getMatchingRule($speciesCount);

            // the species already lives in the cell, and it's environment is fit for survival
            if ($matrix->getMatrix()[$y][$x] === $species && $rule === ReplicationResult::SURVIVE) {
                $speciesToOccupy[] = $species;
            } elseif ($rule === ReplicationResult::REPRODUCE) {
                $speciesToOccupy[] = $species;
            }
        }

        return $speciesToOccupy;
    }

    /**
     * @param int $speciesCount
     * @return ReplicationResult based on following rules
     * If there are two or three organisms of the same type living in the elements surrounding an organism of the same, type then it may SURVIVE.
     * If there are less than two organisms of one type surrounding one of the same type then it will DIE due to isolation.
     * If there are four or more organisms of one type surrounding one of the same type then it will DIE due to overcrowding.
     * If there are exactly three organisms of one type surrounding one element, they may REPRODUCE into that cell.
     */
    private function getMatchingRule(int $speciesCount): ReplicationResult
    {

        return match ($speciesCount) {
            // isolation + overcrowd
            0, 1, 4, 5, 6, 7, 8, 9 => ReplicationResult::DIE,
            3 => ReplicationResult::REPRODUCE,
            2 => ReplicationResult::SURVIVE
        };
    }

}