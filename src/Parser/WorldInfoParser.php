<?php

namespace App\Parser;

use App\Exception\MatrixCreationException;
use App\ValueObject\WorldInfo;
use DOMNode;
use LogicException;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class WorldInfoParser
{

    const DIMENSION = 'dimension';
    const SPECIES_COUNT = 'speciesCount';
    const ITERATIONS_COUNT = 'iterationsCount';
    public function __construct(private readonly ValidatorInterface $validator)
    {
    }

    /**
     * @throws MatrixCreationException
     */
    public function createWorldInfo(DOMNode $worldInfoDocument): WorldInfo
    {

        /** @var DOMNode $childNode */
        foreach ($worldInfoDocument->childNodes as $childNode) {
            switch ($childNode->nodeName) {
                case self::DIMENSION:
                    $dimension = intval($childNode->nodeValue);
                    break;
                case self::SPECIES_COUNT:
                    $speciesCount = intval($childNode->nodeValue);
                    break;
                case self::ITERATIONS_COUNT:
                    $iterationsCount = intval($childNode->nodeValue);
                    break;
                default:
            }
        }
        if (!isset($dimension) || !isset($speciesCount) || !isset($iterationsCount)) {
            throw new LogicException('this exception should not happen');
        }
        $worldInfo = new WorldInfo($dimension, $speciesCount, $iterationsCount);
        $errors = $this->validator->validate($worldInfo);

        if (count($errors) > 0) {
            /** @var ConstraintViolation $firstError */
            $firstError = $errors[0];
            throw new MatrixCreationException($firstError->getMessage());
        }

        return $worldInfo;
    }

}