<?php

namespace App\Parser;

use App\Exception\MatrixCreationException;
use App\ValueObject\Matrix;
use App\ValueObject\Organism;
use App\ValueObject\WorldInfo;
use DOMDocument;
use DOMNode;
use LogicException;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class MatrixParser
{

    const X_POSITION = 'x_pos';
    const Y_POSITION = 'y_pos';
    const SPECIES = 'species';

    public function __construct(
        private readonly ValidatorInterface $validator,
        private readonly WorldInfoParser $worldInfoParser
    ) {
    }

    /**
     * @throws MatrixCreationException
     */
    public function parseMatrix(DOMDocument $world): Matrix
    {

        /** @var DOMNode $worldInfoDocument */
        $worldInfoDocument = $world->getElementsByTagName('world')->item(0);

        /** @var DOMNode $organismsInfoDocument */
        $organismsInfoDocument = $world->getElementsByTagName('organisms')->item(0);

        $worldInfo = $this->worldInfoParser->createWorldInfo($worldInfoDocument);
        $organisms = $this->parseOrganisms($organismsInfoDocument, $worldInfo);
        if (count($organisms['species']) !== $worldInfo->getSpeciesCount()) {
            throw new MatrixCreationException('The number of species does not match the specification');
        }
        $matrixArray = $this->createMatrixArray($worldInfo, $organisms['organisms']);

        return new Matrix($matrixArray, $worldInfo, $organisms['species']);
    }

    /**
     * @param DOMNode $organismsInfoDocument
     * @param WorldInfo $worldInfo
     * @return array{"organisms": Organism[], "species": string[]}
     * @throws MatrixCreationException
     */
    private function parseOrganisms(DomNode $organismsInfoDocument, WorldInfo $worldInfo): array
    {

        $distinctSpecies = [];
        $organisms = [];
        /** @var DOMNode $organismDocument */
        foreach ($organismsInfoDocument->childNodes as $organismDocument) {
            if ($organismDocument->nodeName === '#text') {
                continue;
            }
            /** @var DOMNode $childNode */
            foreach ($organismDocument->childNodes as $childNode) {
                switch ($childNode->nodeName) {
                    case self::X_POSITION:
                        $xPosition = intval($childNode->nodeValue);
                        break;
                    case self::Y_POSITION:
                        $yPosition = intval($childNode->nodeValue);
                        break;
                    case self::SPECIES:
                        $species = (string)$childNode->nodeValue;
                        break;
                    default:
                        break;
                }
            }
            if (!isset($xPosition) || !isset($yPosition) || !isset($species)) {
                throw new LogicException('This exception should not happen. The XML was validated with .xsd');
            }
            $organism = new Organism($xPosition, $yPosition, $species);
            $errors = $this->validator->validate($organism);

            if (count($errors) > 0) {
                /** @var ConstraintViolation $firstError */
                $firstError = $errors[0];
                throw new MatrixCreationException($firstError->getMessage());
            }
            if (!$worldInfo->isInsideWorld($organism->getX(), $organism->getY())) {
                throw new MatrixCreationException(
                    sprintf('Organism on line %d is out of bounds', $organismDocument->getLineNo())
                );
            }
            $organisms[] = $organism;
            if (!in_array($species, $distinctSpecies)) {
                $distinctSpecies[] = $species;
            }

        }

        return ['organisms' => $this->resolveInitialConflicts($organisms), 'species' => $distinctSpecies];
    }

    /**
     * @param Organism[] $organisms
     * @return array
     * if there is more than 1 organism in a cell, randomly decide which one is going to initially occupy the cell
     */
    private function resolveInitialConflicts(array $organisms): array {
        $duplicities = [];
        $uniqueOrganisms = [];
        foreach ($organisms as $organism) {
            $duplicities[sprintf('%d_%d', $organism->getX(), $organism->getY())][] = $organism;
        }
        foreach ($duplicities as &$duplicity) {
            if (count($duplicity) > 1) {
                $uniqueOrganisms[] = $duplicity[array_rand($duplicity)];
                continue;
            }
            $uniqueOrganisms[] = $duplicity[0];
        }
        return $uniqueOrganisms;
    }

    /**
     * @param WorldInfo $worldInfo
     * @param Organism[] $organisms
     * @return array
     */
    private function createMatrixArray(WorldInfo $worldInfo, array $organisms): array
    {

        $row = array_fill(0, $worldInfo->getDimension(), '');
        $matrix = array_fill(0, $worldInfo->getDimension(), $row);

        foreach ($organisms as $organism) {
            $matrix[$organism->getY()][$organism->getX()] = $organism->getType();
        }

        return $matrix;
    }
}