<?php

namespace App\Command;

use App\Exception\MatrixCreationException;
use App\Parser\MatrixParser;
use App\WorldSimulation\WorldSimulator;
use DOMDocument;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 'app:simulate',
    description: 'Simulates game of life for provided XML world specification.',
    hidden: false,
)]
class SimulateCommand extends Command
{

    private array $species;
    private int $sleepMilliseconds;
    private array $backgrounds = ['yellow', 'green'];

    public function __construct(
        private readonly MatrixParser $matrixParser,
        private readonly WorldSimulator $worldSimulator,
        string $name = null
    ) {

        parent::__construct($name);
    }

    protected function configure(): void
    {

        $this
            ->addArgument('file', InputArgument::REQUIRED, 'World XML file')
            ->addOption('milliseconds', 'm', InputOption::VALUE_OPTIONAL, 'Number of seconds between steps', 1000);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {

        $inputFile = $input->getArgument('file');

        $this->sleepMilliseconds = $input->getOption('milliseconds');
        $document = new DOMDocument();
        $document->load($inputFile);

        try {
            $matrix = $this->matrixParser->parseMatrix($document);
            $this->species = $matrix->getSpecies();

            $outputStyle = new OutputFormatterStyle('white', 'white');
            $output->getFormatter()->setStyle('nothing', $outputStyle);
            foreach ($this->species as $index => $singleSpecies) {
                $outputStyle = new OutputFormatterStyle('black', $this->backgrounds[$index % count($this->backgrounds)], ['bold', 'blink']);
                $output->getFormatter()->setStyle('style_' . $index, $outputStyle);
            }
        } catch (MatrixCreationException $e) {
            $output->writeln($e->getMessage());

            return Command::FAILURE;
        }

        $this->printWorld($output, $matrix->getMatrix());
        foreach ($this->worldSimulator->getSteps($matrix) as $step) {
            $this->printWorld($output, $step);
        }

        return Command::SUCCESS;
    }

    private function printWorld(OutputInterface $output, array $matrix)
    {

        foreach ($matrix as $row) {
            foreach ($row as $cell) {
                $output->write($this->mapOutput($cell));
            }
            $output->writeln('');
        }
        $output->writeln('');
        usleep($this->sleepMilliseconds * 1000);
    }

    private function mapOutput($cell): string
    {

        if ($cell === '') {
            return "<nothing>0</nothing> ";
        }

        return $this->formatSpecies($cell);
    }

    private function formatSpecies($species): string
    {
        $key = array_search($species, $this->species);
        return  sprintf('<style_%s>%d</style_%s> ', $key, $key + 1, $key);
    }
}