<?php

namespace App\ValueObject;

use Symfony\Component\Validator\Constraints as Assert;
class WorldInfo
{

    #[Assert\GreaterThan(0)]
    private int $dimension;
    #[Assert\GreaterThan(0)]
    private int $speciesCount;
    #[Assert\GreaterThan(0)]
    private int $iterationsCount;

    public function __construct(int $dimension, int $speciesCount, int $iterationsCount)
    {

        $this->dimension = $dimension;
        $this->speciesCount = $speciesCount;
        $this->iterationsCount = $iterationsCount;
    }

    /**
     * @return int
     */
    public function getDimension(): int
    {

        return $this->dimension;
    }

    /**
     * @return int
     */
    public function getSpeciesCount(): int
    {

        return $this->speciesCount;
    }

    /**
     * @return int
     */
    public function getIterationsCount(): int
    {

        return $this->iterationsCount;
    }

    public function isInsideWorld(int $x, int $y): bool {
        return $x < $this->dimension && $y < $this->dimension && $y >= 0 && $x >= 0;
    }
}