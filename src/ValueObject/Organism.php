<?php

namespace App\ValueObject;

use Symfony\Component\Validator\Constraints as Assert;
class Organism
{

    #[Assert\GreaterThanOrEqual(0)]
    private int $x;

    #[Assert\GreaterThanOrEqual(0)]
    private int $y;

    #[Assert\NotBlank]
    private string $type;

    public function __construct(int $x, int $y, string $type)
    {
        $this->x = $x;
        $this->y = $y;
        $this->type = $type;
    }

    /**
     * @return int
     */
    public function getX(): int
    {

        return $this->x;
    }

    /**
     * @return int
     */
    public function getY(): int
    {

        return $this->y;
    }

    /**
     * @return string
     */
    public function getType(): string
    {

        return $this->type;
    }
}