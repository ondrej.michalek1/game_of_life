<?php

namespace App\ValueObject;

class Matrix
{

    private array $occupiedIndices = [];
    /**
     * @param array $matrix
     * @param WorldInfo $worldInfo
     * @param string[] $species
     */
    public function __construct(
        private array $matrix,
        private readonly WorldInfo $worldInfo,
        private readonly array $species
    ) {
        foreach ($this->matrix as $rowIndex => $row) {
            foreach ($row as $columnIndex => $column) {
                if ($column !== '') {
                    $this->occupiedIndices[] = ['x' => $columnIndex, 'y' => $rowIndex];
                }
            }
        }
    }

    /**
     * @return WorldInfo
     */
    public function getWorldInfo(): WorldInfo
    {

        return $this->worldInfo;
    }

    /**
     * @return array
     */
    public function getMatrix(): array
    {

        return $this->matrix;
    }

    /**
     * @param array $matrix
     */
    public function setMatrix(array $matrix): void
    {

        $this->matrix = $matrix;
    }

    /**
     * @return array
     */
    public function getSpecies(): array
    {

        return $this->species;
    }

    /**
     * @return array
     */
    public function getOccupiedIndices(): array
    {

        return $this->occupiedIndices;
    }

    /**
     * @param array $occupiedIndices
     */
    public function setOccupiedIndices(array $occupiedIndices): void
    {

        $this->occupiedIndices = $occupiedIndices;
    }
    public function getNeighborhood(int $x, int $y): array
    {

        $neighborhood = array_fill_keys($this->getSpecies(), 0);
        $indices = $this->generateNeighborhoodIndices($x, $y);

        foreach ($indices as $index) {

            $species = $this->getMatrix()[$index['y']][$index['x']];
            if ($species !== '') {
                $neighborhood[$species]++;
            }
        }

        return $neighborhood;
    }

    public function generateNeighborhoodIndices(int $x, int $y): array
    {

        $candidates = [
            ['x' => $x, 'y' => $y - 1],
            ['x' => $x, 'y' => $y + 1],
            ['x' => $x - 1, 'y' => $y],
            ['x' => $x - 1, 'y' => $y - 1],
            ['x' => $x - 1, 'y' => $y + 1],
            ['x' => $x + 1, 'y' => $y],
            ['x' => $x + 1, 'y' => $y - 1],
            ['x' => $x + 1, 'y' => $y + 1],
        ];
        $inWorldIndices = [];
        foreach ($candidates as $candidate) {
            if (!$this->getWorldInfo()->isInsideWorld($candidate['x'], $candidate['y'])) {
                continue;
            }
            $inWorldIndices[] = $candidate;
        }

        return $inWorldIndices;
    }
}