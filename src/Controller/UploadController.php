<?php

namespace App\Controller;

use App\Exception\MatrixCreationException;
use App\Form\XMLUploadType;
use App\Parser\MatrixParser;
use App\ValueObject\XMLUpload;
use DOMDocument;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;

class UploadController extends AbstractController
{

    #[Route(path: '/', name: 'index')]
    public function processXML(Request $request, MatrixParser $matrixParser): Response
    {

        $xmlUpload = new XMLUpload();
        $form = $this->createForm(XMLUploadType::class, $xmlUpload);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $xmlDocument = new DOMDocument();
            $xmlDocument->load($xmlUpload->getWorld());

            try {
                $matrix = $matrixParser->parseMatrix($xmlDocument);
            } catch (MatrixCreationException $e) {
                throw new BadRequestHttpException($e);
            }
        }

        return $this->render('index.html.twig', ['form' => $form->createView()]);
    }
}