<?php

namespace App\Enum;

enum ReplicationResult
{
    case SURVIVE;
    case DIE;
    case REPRODUCE;
}