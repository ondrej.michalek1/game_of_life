<?php

namespace App\Validator;

use DOMDocument;
use LogicException;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class WorldSchemaConstraintValidator extends ConstraintValidator
{

    public function __construct(private readonly string $schemaFilePath)
    {
    }

    /**
     * @param File $value
     * @param Constraint $constraint
     * @return void
     */
    public function validate($value, Constraint $constraint): void
    {
        if (!($value instanceof File)) {
            throw new LogicException('This constraint is for files only');
        }

        if (!($constraint instanceof WorldSchemaConstraint)) {
            throw new LogicException('Wrong constraint');
        }

        $xmlDocument = new DOMDocument();
        $xmlDocument->load($value);

        if (!$xmlDocument->schemaValidate($this->schemaFilePath)) {
       //     $this->context->buildViolation($constraint->message);
        }
    }
}